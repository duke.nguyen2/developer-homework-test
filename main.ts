import {
  GetBaseUoM,
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  NutrientFact,
  Product,
  Recipe,
  SupplierProduct,
  UnitOfMeasure,
} from "./supporting-files/models";
import {
  ConvertUnits,
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

// New data structure to indicate product with the cheapest supplier
interface CheapestSupplier {
  productName: string;
  brandName: string;
  nutrientFacts: NutrientFact[];
  supplierProduct: SupplierProduct;
}

/* 
Get the product with cheapest supplier
Another approach: we can iterate the suppliers to find the cheapest one, it can be better for performance, but not clean as this way.
Ref: https://gitlab.com/duke.nguyen2/developer-homework-test/-/tree/feature/update-get-cheapest-supplier
*/
const getCheapestSupplier = (products: Product[]): CheapestSupplier => {
  const suppliers = products.flatMap((p) =>
    p.supplierProducts.map((s) => ({
      productName: p.productName,
      brandName: p.brandName,
      nutrientFacts: p.nutrientFacts,
      supplierProduct: s,
    }))
  );
  return suppliers.sort(
    (s1, s2) =>
      GetCostPerBaseUnit(s1.supplierProduct) -
      GetCostPerBaseUnit(s2.supplierProduct)
  )[0];
};

// Calculate the cheapest price base on the cost per base unit and base amount
const calculateCheapestPrice = (
  recipeName: string,
  cheapestSupplier: CheapestSupplier,
  convertedUnit: UnitOfMeasure
) => {
  recipeSummary[recipeName].cheapestCost +=
    GetCostPerBaseUnit(cheapestSupplier.supplierProduct) *
    convertedUnit.uomAmount;
};

// Calculate the nutrition base on the nutrient face in base unit and base amount
const calculateNutrition = (
  recipeName: string,
  cheapestSupplier: CheapestSupplier,
  convertedUnit: UnitOfMeasure
) => {
  cheapestSupplier.nutrientFacts.forEach((n) => {
    const currentNutrition =
      recipeSummary[recipeName].nutrientsAtCheapestCost[n.nutrientName];

    const nutrientFactBaseUnit = GetNutrientFactInBaseUnits(n);

    const quantityAmount =
      (nutrientFactBaseUnit.quantityAmount.uomAmount /
        nutrientFactBaseUnit.quantityPer.uomAmount) *
      convertedUnit.uomAmount;

    // Init the nutrient value if it's not in summary, otherwise update the uomAmount only
    if (!currentNutrition) {
      recipeSummary[recipeName].nutrientsAtCheapestCost[n.nutrientName] = {
        nutrientName: nutrientFactBaseUnit.nutrientName,
        quantityAmount: {
          ...nutrientFactBaseUnit.quantityAmount,
          uomAmount: quantityAmount,
        },
        quantityPer: {
          ...nutrientFactBaseUnit.quantityPer,
        },
      };
    } else {
      recipeSummary[recipeName].nutrientsAtCheapestCost[
        n.nutrientName
      ].quantityAmount.uomAmount += quantityAmount;
    }
  });
};

// Calculate the cheapest cost and summary nutrition for each recipe
const summaryCostAndNutrition = (recipe: Recipe) => {
  recipeSummary[recipe.recipeName] = {
    cheapestCost: 0,
    nutrientsAtCheapestCost: {},
  };

  recipe.lineItems.forEach((l) => {
    const productsForItem = GetProductsForIngredient(l.ingredient);
    const cheapestSupplier = getCheapestSupplier(productsForItem);

    // Convert measure of recipe to the base unit
    const baseUnitOfMeasure = GetBaseUoM(l.unitOfMeasure.uomType);
    const convertedUnit = ConvertUnits(
      l.unitOfMeasure,
      baseUnitOfMeasure.uomName,
      baseUnitOfMeasure.uomType
    );

    calculateCheapestPrice(recipe.recipeName, cheapestSupplier, convertedUnit);
    calculateNutrition(recipe.recipeName, cheapestSupplier, convertedUnit);
  });
};

recipeData.forEach((r) => summaryCostAndNutrition(r));

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
